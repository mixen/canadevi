// core version + navigation, pagination modules:
import Swiper from "swiper";
import { Autoplay, Navigation } from "swiper/modules";
// import Swiper and modules styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/autoplay";

// init Swipers:
const swiperJumbotron = new Swiper(".swiper-jumbotron", {
  // configure Swiper to use modules
  modules: [Navigation, Autoplay],
  autoplay: {
    delay: 4000,
    disableOnInteraction: false,
  },

  // Navigation arrows
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

swiperJumbotron.on("slideChange", function () {
  // Remove the Animate.css classes from all elements
  let allThumbArtistas = document.querySelectorAll(".thumb-artista");
  let allPresentandos = document.querySelectorAll(".presentando");
  let allNombreArtistas = document.querySelectorAll(".nombre-artista");
  let allInfos = document.querySelectorAll(".contenedor-info");

  allThumbArtistas.forEach(function (thumbArtista) {
    thumbArtista.classList.remove("animate__animated", "animate__fadeInUp");
  });

  allPresentandos.forEach(function (presentando) {
    presentando.classList.remove("animate__animated", "animate__fadeInLeft");
  });

  allNombreArtistas.forEach(function (nombreArtista) {
    nombreArtista.classList.remove("animate__animated", "animate__fadeInDown");
  });

  allInfos.forEach(function (info) {
    info.classList.remove("animate__animated", "animate__fadeInUp");
  });

  // Add the Animate.css classes only to the elements within the current slide
  let currentSlide = swiperJumbotron.slides[swiperJumbotron.activeIndex];
  let thumbArtistasInCurrentSlide =
    currentSlide.querySelectorAll(".thumb-artista");
  let presentandosInCurrentSlide =
    currentSlide.querySelectorAll(".presentando");
  let nombreArtistasInCurrentSlide =
    currentSlide.querySelectorAll(".nombre-artista");
  let infosInCurrentSlide = currentSlide.querySelectorAll(".contenedor-info");

  thumbArtistasInCurrentSlide.forEach(function (thumbArtista) {
    thumbArtista.classList.add("animate__animated", "animate__fadeInUp");
  });

  presentandosInCurrentSlide.forEach(function (presentando) {
    presentando.classList.add("animate__animated", "animate__fadeInLeft");
  });

  nombreArtistasInCurrentSlide.forEach(function (nombreArtista) {
    nombreArtista.classList.add("animate__animated", "animate__fadeInDown");
  });

  infosInCurrentSlide.forEach(function (info) {
    info.classList.add("animate__animated", "animate__fadeInUp");
  });
});

const swiperPatrocinadores = new Swiper(".swiper-patrocinadores", {
  // configure Swiper to use modules
  modules: [Autoplay],

  speed: 1750,
  centeredSlides: true,
  spaceBetween: 30,
  slidesPerView: 2,
  loop: true,
  autoplay: {
    delay: 250,
  },
  noSwiping: true,
  noSwipingClass: "no-swiping",
  breakpoints: {
    576: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 4,
    },
    992: {
      slidesPerView: 6,
    },
    1200: {
      slidesPerView: 8,
    },
  },
});

// Fetch JSON data
fetch("https://feeds.behold.so/wtyicTxpQfpEU0ZaoKDJ")
  .then((response) => response.json())
  .then((data) => {
    // Extract posts array from JSON
    const posts = data.posts;

    // Select the swiper-wrapper element of the specific SwiperJS slider
    const swiperWrapper = document.querySelector(
      ".swiper-asi-se-vive .swiper-wrapper",
    );

    // Loop through each post and create a swiper-slide with the image
    posts.forEach((post) => {
      // Get the medium-sized image URL
      const imageUrl = post.sizes.medium.mediaUrl;

      // Create swiper-slide element
      const swiperSlide = document.createElement("div");
      swiperSlide.classList.add("swiper-slide");

      // Create anchor element
      const anchor = document.createElement("a");
      anchor.href = post.permalink;
      anchor.target = "_blank"; // Open link in new tab

      // Create image element
      const img = document.createElement("img");
      img.src = imageUrl;
      img.alt = post.caption;
      img.classList.add("thumb-artista", "img-fluid");
      img.setAttribute("loading", "lazy");

      // Append image to anchor element
      anchor.appendChild(img);

      // Append anchor element to swiper-slide
      swiperSlide.appendChild(anchor);

      // Append swiper-slide to swiper-wrapper
      swiperWrapper.appendChild(swiperSlide);
    });

    // Initialize the specific SwiperJS slider after adding slides
    new Swiper(".swiper-asi-se-vive", {
      // configure Swiper to use modules
      modules: [Navigation, Autoplay],
      centeredSlides: true,
      spaceBetween: 30,
      slidesPerView: 1,
      loop: true,
      autoplay: {
        delay: 4000,
        disableOnInteraction: false,
      },
      breakpoints: {
        576: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 2,
        },
        992: {
          slidesPerView: 3,
        },
        1200: {
          slidesPerView: 3,
        },
      },

      // Navigation arrows
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
  })
  .catch((error) => console.error("Error fetching JSON:", error));
