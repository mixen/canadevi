// Libraries
window.bootstrap = require("bootstrap/dist/js/bootstrap.bundle.js");

import "../src/smooth-scrolling";
import "../src/form-ajax";
import "../src/aos";
import "../src/swipers";
import "animate.css";
import Isotope from "isotope-layout";
import "@lottiefiles/lottie-player";

// Header
document.addEventListener("DOMContentLoaded", function () {
  // Cache the DOM element containing the navbar
  var header = document.getElementById("navbar");

  function updateScroll() {
    var scroll = window.pageYOffset || document.documentElement.scrollTop;

    if (scroll >= 1) {
      header.classList.add("navbar-scroll");
    } else {
      header.classList.remove("navbar-scroll");
    }
  }

  window.addEventListener("scroll", updateScroll);
  updateScroll();
});

// Menú de navegación
$("#mburger").click(function (e) {
  e.stopPropagation();
  $(".menu").toggleClass("menu-abierto");
  $("#navbar").toggleClass("opacity-0");
  $("#backdrop").toggleClass("backdrop-opacity-1");
});

$(".menu").click(function (e) {
  e.stopPropagation();
});

$("body,html").click(function (e) {
  $(".menu").removeClass("menu-abierto");
  $("#navbar").removeClass("opacity-0");
  $("#backdrop").removeClass("backdrop-opacity-1");
});

const btnCerrarMenu = document.getElementById("cerrar-menu");
const btnLogo = document.getElementById("btn-logo");
const btnNav1 = document.getElementById("btn-nav-1");
const btnNav2 = document.getElementById("btn-nav-2");
const btnNav3 = document.getElementById("btn-nav-3");
const btnNav4 = document.getElementById("btn-nav-4");
const btnNav5 = document.getElementById("btn-nav-5");
const btnNav6 = document.getElementById("btn-nav-6");
const btnNav7 = document.getElementById("btn-nav-7");

if (btnCerrarMenu) {
  btnCerrarMenu.addEventListener("click", cerrarMenu, false);
}

if (btnLogo) {
  btnLogo.addEventListener("click", cerrarMenu, false);
}

if (btnNav1) {
  btnNav1.addEventListener("click", cerrarMenu, false);
}

if (btnNav2) {
  btnNav2.addEventListener("click", cerrarMenu, false);
}

if (btnNav3) {
  btnNav3.addEventListener("click", cerrarMenu, false);
}

if (btnNav4) {
  btnNav4.addEventListener("click", cerrarMenu, false);
}

if (btnNav5) {
  btnNav5.addEventListener("click", cerrarMenu, false);
}

if (btnNav6) {
  btnNav6.addEventListener("click", cerrarMenu, false);
}

if (btnNav7) {
  btnNav7.addEventListener("click", cerrarMenu, false);
}

function cerrarMenu() {
  $(".menu").removeClass("menu-abierto");
  $("#navbar").removeClass("opacity-0");
  $("#backdrop").removeClass("backdrop-opacity-1");
}

// Cerrar menú con Esc
document.addEventListener("keydown", (event) => {
  if (event.key === "Escape") {
    cerrarMenu();
  }
});
